# c-ares

## 简介
> c-ares是异步解析器库，适用于需要无阻塞地执行 DNS 查询或需要并行执行多个 DNS 查询的应用程序。

![](screenshot/result.gif)

## 下载安装
直接在OpenHarmony-SIG仓中搜索c-ares并下载。

## 使用说明
以OpenHarmony 3.1 Beta的rk3568版本为例

1. 将下载的c-ares库代码存在以下路径：./third_party/c-ares

2. 修改添加依赖的编译脚本，路径：/developtools/bytrace_standard/ohos.build

```

{
  "subsystem": "developtools",
  "parts": {
    "bytrace_standard": {
      "module_list": [
        "//developtools/bytrace_standard/interfaces/innerkits/native:bytrace_core",
        "//developtools/bytrace_standard/bin:bytrace_target",
        "//developtools/bytrace_standard/bin:bytrace.cfg",
        "//developtools/bytrace_standard/interfaces/kits/js/napi:bytrace",
        "//third_party/c_ares:c_ares_target"
      ],
      "inner_kits": [
        {
          "type": "so",
          "name": "//developtools/bytrace_standard/interfaces/innerkits/native:bytrace_core",
          "header": {
            "header_files": [
              "bytrace.h"
            ],
            "header_base": "//developtools/bytrace_standard/interfaces/innerkits/native/include"
          }
        }
      ],
      "test_list": [
        "//developtools/bytrace_standard/bin/test:unittest"
      ]
    }
  }
}

```

3. 编译：./build.sh --product-name rk3568 --ccache

4. 生成库文件和一些可执行测试文件，路径：out/rk3568/developtools/profiler

## 接口说明
1. ares对channel进行初始化：
   `ares_init()`
2. 销毁channel，释放内存，关闭打开的socket：
   `ares_destroy(self.channel)`
3. 初始化channel,options，optmask：
   `ares_init_options(&channel, &options, optmask)`
4. 设置dns请求服务器，设置完成需要free掉c_servers的内存空间：
   `ares_set_servers(self.channel, cares.ares_addr_node* c_servers)`
5. ares处理完成，返回DNS解析的信息：
   `dns_callback()`
6. 获取ares channel使用的FD：
   `ares_fds()`
7. 有事件发生，交由ares处理：
   `ares_process()`
8. 传递给c-ares channal和回调：
   `ares_gethostbyaddr()`

## 兼容性
支持OpenHarmony API version 8 及以上版本。

## 目录结构
````
|---- c-ares
|     |---- ci
|     |---- docs                         #资源文件
|     |---- include                      #头文件
|     |---- m4                           #预处理文件
|     |---- src
|           |---- lib
|                 |---- ares__close_sockets.c   #关闭TCP和UDP连接
|                 |---- ares__get_hostent.c     #获取area hostent
|                 |---- ares__read_line.c       #将文件中的一行读取到动态分配的缓冲区
|                 |---- ares__readaddrinfo.c    #读取地址信息
|                 |---- ares__sortaddrinfo.c    #对地址进行排序，并重新排列链表
|                 |---- ares_cancel.c           #取消请求
|                 |---- ares_fds.c              #获取使用的FD
|                 |---- ares_search.c           #area搜索
|     |---- test                         #单元测试文件
|     |---- README.md                    #安装使用方法
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/c-ares/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/c-ares/pulls) 。

## 开源协议
本项目基于 [MIT license](https://gitee.com/openharmony-sig/c-ares/blob/master/LICENSE.md) ，请自由地享受和参与开源。
